// Incluimos gulp
var gulp = require('gulp');

// Incluimos los plugins
var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    util = require('gulp-util'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    rename = require('gulp-rename'),
    nodemon = require('gulp-nodemon'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    browserify = require('gulp-browserify'),
    aliasify = require('aliasify'),
    vueify = require('gulp-vueify'),
    cvueify = require('vueify'),
    babelify = require('babelify');

// Tarea para chequear sintaxis JS
gulp.task('lint', function() {
    return gulp.src('source/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compilamos el Sass
gulp.task('sass', function() {
    return gulp.src('source/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css'));
});

// Precompilamos archivos vue
gulp.task('vueify', function(){
    return gulp.src('source/components/*.vue')
        .pipe(vueify())
        .pipe(gulp.dest('source/js'))
        .pipe(rename('template.js'))
        .pipe(livereload());
});
// Concatenamos y minificamos el JS
gulp.task('scripts', ['lint'], function() {
    
    return gulp.src('source/js/*.js')
        .pipe(concat('app.js'))
        .pipe(browserify({
            ext: ['*.js','*.vue'],
            insertGlobals: true,
            debug : true,
            transform: [[babelify,{_flags:{debug:true}},cvueify]]
        }))
        .pipe(gulp.dest('dist'))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .on('error', function(err) { util.log(util.colors.red('[Error]'), err.toString());})
        .pipe(gulp.dest('public/libs'))
        .pipe(livereload());
});

// Observamos los archivos para cambios
gulp.task('watch', function() {
    gulp.watch('js/components/*.vue', ['vueify']);
    gulp.watch('js/*.js', ['lint', 'script']);
    gulp.watch('scss/*.scss', ['sass']);
});
//Default Task
gulp.task('default', [ 'lint', 'sass', 'scripts', 'watch'], function() {

    livereload.listen();
    nodemon({
        script: 'server/main.js',
        ext: 'js html'
    })
        .on('restart', function() {
            gulp.src('server/main.js')
                .pipe(notify('recargando pagina, espera porfavor...'))
                .pipe(livereload());
        });
});