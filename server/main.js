//Declaramos las variables del Servidor la Aplicacion y socket
var express = require('express'),
    app = express(),
    server = require('http').Server(app),
    io = require('socket.io').listen(server),
    path = require('path');

// declaramos el arreglo de prueba
var messages = [{
        id: 1,
        text: "Hola soy un mensaje",
        author: "Ramses Salaza"
}]

app.use('/vue', express.static(path.join(__dirname , '/../node_modules/vue/dist')));

app.use(express.static('public'))


app.get('/', function(res, req){
    res.status(200).sendFile(__dirname +'/public/index.html');
});

app.get('/hello', function(req, res){
    res.status(200).send("¡Hola Mundo!");
});

io.on('connection', function(socket) {
    console.log('alguien se ha conectado con Socket: '+socket.id);


    socket.emit('messages', messages);

    socket.on('send-message', function(data){
        data.author = data.author +"/"+ socket.id;
        messages.push(data);
        if(data.code){
            io.to(data.code).emit('messages', messages);
        }else{
            io.sockets.emit('messages', messages);
        }
    });


});

// inicializamos el servidor
server.listen(8080, function() {
    console.log(__dirname);
    console.log("Servidor corriendo en http://localhost:8080");
});