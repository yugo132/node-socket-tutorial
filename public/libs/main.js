var socket = io.connect('http://localhost:8080', {'forceNew': true });

socket.on('messages', function(data) {
    console.log(data);
    render(data);
})

function render(data) {
    var html = data.map(function(data,index){
        return(`<div>
                    <strong>${data.author}</strong>
                    <em>${data.text}</em>
                </div>`);
    }).join(" ");

    document.getElementById('messages').innerHTML = html;

}

function sendMessage(e) {
    var payload = {
        code: document.getElementById('code').value,
        author: document.getElementById('username').value,
        text: document.getElementById('text').value
    };
    console.log(payload);
    socket.emit('send-message', payload);

    return false;
}