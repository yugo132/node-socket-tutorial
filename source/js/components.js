/*jshint esversion: 6 */
var Vue = require('vue');
var Test = require('./components/test.vue');
const app = new Vue({
    el: '#send-message',
    data: {
        message: 'Working Vue!'
    },
    methods: {
        send: function(event) {
            event.preventDefault();
            console.log('prevenido');
            this.message = "Mensaje Enviado!";
            sendMessage(event);
        }
    }
});
const test = new Vue({
    el: '#test',
    render: function(createElement) {
        return createElement(Test);
    }
}); 
